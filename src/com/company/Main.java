package com.company;

import java.util.concurrent.Semaphore;

public class Main {
    static int balance = 0;
    static  volatile boolean check =true;

    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    while (check) {
                        balance += 10;
                        System.out.println(balance);
                        check=false;
                    }
                }

            }

        });
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    while (!check) {
                        balance -= 10;
                        System.out.println(balance);
                        check = true;
                    }

                }
            }
        });
        thread.start();
        thread1.start();
    }
}
